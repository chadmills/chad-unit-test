<?php
use PHPUnit\Framework\TestCase;
require __DIR__ . '/../src/ItemDescription.php';


class ItemDescriptionTest extends TestCase{
    public $itemDescriptionInstance;

    protected function setUp(): void {
        $this->itemDescriptionInstance = new ItemDescription();
    }

    public function testIfWorks(){
        $this->assertEquals(true, $this->itemDescriptionInstance->generateDescription(array()));
    }
}//END class ItemDescriptionTest